package de.vogella.jpa.simple.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.vogella.jpa.simple.model.Todo;


public class Run{

	private static final String PERSISTENCE_UNIT_NAME = "todos";
    private static EntityManagerFactory factory;

    public static void main(String[] args) {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        
        // create new todo
        em.getTransaction().begin();
        Todo todo = new Todo();
        todo.setSummary("Test");
        todo.setDescription("Test");
        em.persist(todo);
        em.getTransaction().commit();
        
        // read the existing entries and write to console
        Query q = em.createQuery("select t from Todo t");
        List<Todo> todoList = q.getResultList();
        int i = 0;
        for (Todo to : todoList) {
            System.out.println("Objekt " + (i+1) + ": " + to.toString());
            i++;
        }
        System.out.println("Size: " + todoList.size());

        em.close();

    }
}
